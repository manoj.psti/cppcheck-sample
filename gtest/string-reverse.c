/*----------------------------------------------------------------------------
 * Name:    Blinky.c
 * Purpose: LED Flasher
 * \copyright Copyright (c) 2017 Keil - An ARM Company. All rights reserved.
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

// function definition of the revstr()
char * revstr(char *str1) {
    // declare variable
    int i, len, temp;
    len = strlen(str1);  // use strlen() to get the length of str string
    // use for loop to iterate the string
    for (i = 0; i < len / 2; i++) {
        // temp variable use to temporary hold the string
        temp = str1[i];
        str1[i] = str1[len - i - 1];
        str1[len - i - 1] = temp;
    }
    return str1;
}
int main1() {
    int x = 1;
    char str[50];  // size of char string
    printf(" Enter the string: ");
    scanf("%s", str);  // use gets() function to take string

    printf(" \n Before reversing the string: %s \n", str);

    // call revstr() function
    revstr(str);
    printf(" After reversing the string: %s", str);
    if (x == 1) {
        printf("X = 1");
    } else if (x == 1) {
        printf("x == 1");
    } else {
        // Do Nothing
    }

    return 0;
}
