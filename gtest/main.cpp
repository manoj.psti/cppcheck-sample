#include "gtest/gtest.h"
#include "string-reverse.c"

char str[5] = "rtp";

TEST(revstr, ok)
{
    EXPECT_EQ(str, revstr(str));
};

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}