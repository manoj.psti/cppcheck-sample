/* Rule 15.6: The body of an iteration statement or selection-statement shall 
 * be a compound statement
 */

# include <stdint.h>

static int x; // Rule 8.9: An object should be defined at block scope if its identifier only appears in single function
x = 10;
static int y;
y = 20;


int main(int argc, char **argv) {
    
    if ( x == 20U )                                 /* Non Compliant */
        printf("X = 20");                           /* Non Compliant */
    else                                            /* Non Compliant */ 
        printf("x = %d", x); // Print value of x    /* Non Compliant */ 

    while (x >= 20U)
        printf("x >= 20");

    /* Compliant */
    if (x == 20) { // Rule 7.2 u or U to be suffixed after the unsigned const integers
        printf("X = 20");    // Print x = 20 
    } else {          
        printf("x = %d", x); // Print value of x    
    }

    /* Compliant */
    while (x >= 20U) {
        printf("x >= 20");
    }
}

