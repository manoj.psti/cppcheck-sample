// As per rule number 21.6 Standard library input / output functions shall not be used
#include <stdio.h>
#include <stdint.h>

/* Rule 14.4 The controlling expression of if statement and the controlling 
 * expression of iteration-statement shall have essentially Boolean type
*/
// Use of brackets while defining a macro
#define ONE 1

static void functionRule14_2(void);

// int argc, char** argv
int main(int argc, char **argv){

}

static void functionRule14_2(void){
    uint8_t *x, *y; // Rule 12.3 comma seperator shall not be used 

    // Voilation
    while ( x ) { 
        /* Nothing to do here */
    }
    // compliant code
    while ( y != ONE) { 
        /* Nothing to do here */
    }

    uint16_t i;

    // Voilation
    if ( i ) { 
        /* Do Something */
    } else {
        // Do Nothing
    }
    
}