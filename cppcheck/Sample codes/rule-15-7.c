/* Rule 15.7: All if... else if constructs shall be terminated with an else 
 * statement 
 */

# include <stdint.h>

#define TWENTY (20)

static uint8_t x; // Rule 8.9: An object should be defined at block scope if its identifier only appears in single function
x = 10;
static uint8_t y;
y = 20;


int main(int argc, char **argv) {
    
    if ( x == 20U) {                                /* Non Compliant */
        printf("X = 20");                             /* Non Compliant */
    } 

    /* Non Compliant */
    if (x == 20U) { 
        printf("X = 20");    // Print x = 20 
    } else  if (x <= 20U) {          
        printf("x = %d", x); // Print value of x    
    }


}