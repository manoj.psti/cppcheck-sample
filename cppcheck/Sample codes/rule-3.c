/* Rule 3.1: The character sequence /* and // shall not be used within a comment */
/* Rule 3.2: Line Splicing shall not be used in // comments*/

#include <stdint.h>

int main(int argc, char const *argv[])
{
    uint8_t condition = 8U;
    // Comment /* Rule Voilation */
    // if condition comment \
    if (condition <= 8U) {
        
    } else {
        // Do Nothing
    }

    return 0;
}
