/* Rule 15.5 : A function should have a single point of exit at the end */
#include <stdint.h>

uint8_t functionF(uint8_t input);
uint8_t functionCompliant(uint8_t input);

int main (char argc, char ** argv){

}

// Non Compliant code
uint8_t functionF(uint8_t input) {
    if (input == NULL) {
        return 10;
    } else {
        return 20;
    }
}

// Compliant code
uint8_t functionCompliant(uint8_t input)
{
    uint8_t ret_val;
    if (input == NULL) {
        ret_val = 10;
    } else {
        ret_val = 20;
    }
    return ret_val;
}