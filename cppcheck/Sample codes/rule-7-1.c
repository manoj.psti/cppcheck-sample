/* Rule 7.1: Octal Constants shall not be used */
/* Rule 7.2: A 'u' or 'U' suffix shall be applied to all the integer constants
 * that represent in an unsigned type
 */

#include <stdint.h>

static uint16_t array[6];

int main(char argc, char **argv) {
    array[0] = 99;
    array[1] = 0xff;
    array[2] = 077;
    array[3] =  77;
    array[4] = 052;
    array[5] = 99;
}

