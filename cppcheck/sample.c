// #include <stdio.h>
void foo_bar(int input);

int main(int argc, char **argv) {
	foo_bar(10);
	return 0;
}

void foo_bar(int input)
{
    int buf[10];
	// Comment 
    if (input == 1000){
        buf[input] = 0; // <- ERROR
		/* The Value returned by a function having non-void return type shall be used */
		printf("Print");  //Rule 17.7 
	} 
}